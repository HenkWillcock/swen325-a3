import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { GlobalVariablesProvider } from '../../providers/global-variables/global-variables';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Platform } from 'ionic-angular';
import { ChartsModule } from 'ng2-charts';

declare var Paho: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private lastSeenLocation: string = 'None';
  private timeLastSeen: Date = new Date(0);
  private minutesSinceLastMotion: number = 0;

  constructor(
    public navCtrl: NavController,
    public globals: GlobalVariablesProvider,
    public push: Push,
    public platform: Platform,
    public alertController: AlertController) {
  }

  public chartLabels: string[] = ['Living', 'Dining', 'Toilet', 'Kitchen', 'Bedroom']
  public chartData: number[] = [1, 0, 0, 0, 0];
  public chartType: string = 'bar';

  public startClock = () => {
    console.log('Clock Started');
    this.updateMinutesSinceLastMotion();
    window.setInterval(this.updateMinutesSinceLastMotion, 30000)
  }

  public updateMinutesSinceLastMotion = async () => {
    var currentDate: Date = new Date();
    console.log('Current Date:')
    console.log(currentDate)
    this.minutesSinceLastMotion = await Math.ceil((currentDate.getTime() - this.timeLastSeen.getTime())/60000);
    if(this.minutesSinceLastMotion == 5) {
      this.createPushNotification();
    }
  }

  ionViewDidLoad() {
    this.startClock();
  }

  public connect = () => {
    this.globals.mqttStatus = 'Connecting...';
    this.globals.mqttClient = new Paho.MQTT.Client('barretts.ecs.vuw.ac.nz', 8883, '/mqtt', this.globals.clientId)
    this.globals.mqttClient.onConnectionLost = this.onConnectionLost;
    this.globals.mqttClient.onMessageArrived = this.onMessageArrived;

    console.log('1: Connecting to MQTT via Websocket')

    this.globals.mqttClient.connect({
      timeout:10, 
      useSSL:false,
      onSuccess:this.onSuccess, 
      onFailure:this.onFailure
    })
  }

  public disconnect = () => {
    if(this.globals.mqttStatus == 'Connected') {
      this.globals.mqttStatus = 'Disconnecting...'
      this.globals.mqttClient.disconnect();
      this.globals.mqttStatus = 'Disconnected';
    } else {
      console.log('Not Connected')
    }
  }

  public onSuccess = () => {
    console.log('Connected');
    this.globals.mqttStatus = 'Connected';

    this.globals.mqttClient.subscribe(this.globals.topic);
  }

  public onFailure = () => {
    console.log('Failed to Connect')
    this.globals.mqttStatus = 'Failed to Connect'
  }

  public onConnectionLost = (responseObject) => {
    if(responseObject.errorCode !== 0) {
      this.globals.mqttStatus = 'Disconnected';
    }
  }

  public onMessageArrived = async (message) => {
    var messageStr:string = message.payloadString;
    console.log('2: Received Message')
    this.globals.lastMessageReceived = messageStr;

    if(this.isSeniorData(messageStr)) {
      //Get Info
      var arrayFromMessage = await messageStr.split(',');
      var timestamp = arrayFromMessage[0]; //Format: YYYY-MM-DD hh:mm:ss
      var sensor_location = arrayFromMessage[1];
      var motion_was_sensed: number = +arrayFromMessage[2];
      var battery_charge = await parseInt(arrayFromMessage[3]);
      
      //Update Last Seen Position And Time
      if(motion_was_sensed == 1) {
        this.updateLastSeenTime(timestamp, sensor_location);
        this.lastSeenLocation = sensor_location.charAt(0).toUpperCase() + sensor_location.slice(1);
        this.addToHistogram(sensor_location);
      }

      //Update Battery Statuses
      this.updateBatteryCharge(sensor_location, battery_charge);
    }
  }

  public addToHistogram = (sensor_location) => {
    if(sensor_location == 'living') {this.chartData[0]++} 
    else if(sensor_location == 'kitchen') {this.chartData[1]++;}
    else if(sensor_location == 'dining') {this.chartData[2]++;} 
    else if(sensor_location == 'toilet') {this.chartData[3]++;} 
    else if(sensor_location == 'bedroom') {this.chartData[4]++;}
  }

  public isSeniorData = (messageStr) => {
    return messageStr.includes(',');
    //Improve this
  }

  public updateLastSeenTime = async (timestamp, sensor_location) => {
    var timestampSplit = await (timestamp+'').split(' ');
    
    var date = timestampSplit[0];
    var time = await timestampSplit[1];

    var dateSplit = (date+'').split('-')
    var timeSplit = await (time+'').split(':')

    var year = +dateSplit[0];
    var month = +dateSplit[1];
    var day = +dateSplit[2];

    var hour = +timeSplit[0];
    var minute = +timeSplit[1];
    var second = await +timeSplit[2];

    var currentTime = await new Date(year, month, day, hour, minute, second, 0)

    if(currentTime > this.timeLastSeen) {
      this.timeLastSeen = currentTime;
      console.log('Time updated to ', this.timeLastSeen)
      this.updateMinutesSinceLastMotion();
    }
  }

  public updateBatteryCharge = (sensor_location: string, battery_charge: number) => {
    if(sensor_location == 'living') {this.globals.livingRoomBattery = battery_charge;} 
    else if(sensor_location == 'kitchen') {this.globals.kitchenBattery = battery_charge}
    else if(sensor_location == 'dining') {this.globals.diningRoomBattery = battery_charge} 
    else if(sensor_location == 'toilet') {this.globals.toiletBattery = battery_charge} 
    else if(sensor_location == 'bedroom') {this.globals.bedroomBattery = battery_charge}
  }

  public createPushNotification = () => {
    
    if (this.platform.is('core')) {
      // Computers don't support push notifications so don't do it.
      console.log('Push Notifcation Would\'ve gone off but you\'re on a browser. Instead you get an alert.');
      let alert = this.alertController.create({
        title: 'Warning',
        subTitle: 'It\'s been 5 minutes since your senior has been detected moving.',
        buttons: ['Dismiss']
      })
      alert.present();
    } else {
      this.push.hasPermission().then((res: any) => {
        if (res.isEnabled) {
          console.log('Permission granted');
          this.push.createChannel({
          id: "testchannel1",
          description: "My first test channel",
          importance: 3
          }).then(() => console.log('Channel created'));

          this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

          this.push.listChannels().then((channels) => console.log('List of channels', channels))

          const options: PushOptions = {
            android: {},
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
          };

          const pushObject: PushObject = this.push.init(options);

          pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

          pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));

          pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
        } else {
          console.log('Permission denied');
        }
      });

      this.push.createChannel({
      id: "testchannel1",
      description: "My first test channel",
      importance: 3
      }).then(() => console.log('Channel created'));

      this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

      this.push.listChannels().then((channels) => console.log('List of channels', channels))

      const options: PushOptions = {
        android: {},
        ios: {
            alert: 'true',
            badge: true,
            sound: 'false'
        },
        windows: {},
        browser: {
            pushServiceURL: 'http://push.api.phonegap.com/v1/push'
        }
      };

      const pushObject: PushObject = this.push.init(options);

      pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

      pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));

      pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
    }
  }
}
