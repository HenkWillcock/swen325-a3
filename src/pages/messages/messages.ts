import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { GlobalVariablesProvider } from '../../providers/global-variables/global-variables';

/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var Paho: any;

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {

  private messageToSend: string = 'Your message';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public toastController: ToastController,
    public globals: GlobalVariablesProvider) {
  }

  public sendMessage = () => {
    if(this.globals.mqttStatus == 'Connected') {
      this.globals.mqttClient.publish(this.globals.topic, this.messageToSend);
      this.presentToast('Message Sent to the Broker')
    } else {
      this.presentToast('Not Connected to the Broker')
    }
  }

  public presentToast = (message: string) => {
    let toast = this.toastController.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
  }

}
