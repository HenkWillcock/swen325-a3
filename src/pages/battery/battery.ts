import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVariablesProvider } from '../../providers/global-variables/global-variables';

/**
 * Generated class for the BatteryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-battery',
  templateUrl: 'battery.html',
})
export class BatteryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public globals: GlobalVariablesProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BatteryPage');
  }

}
