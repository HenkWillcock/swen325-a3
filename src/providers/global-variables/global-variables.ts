
import { Injectable } from '@angular/core';

import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

/*
  Generated class for the GlobalVariablesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalVariablesProvider {

  public mqttStatus: string = "Disconnected";
  public mqttClient: any = null;
  public topic: string = 'swen325/t3';
  public clientId: string = 'Henk Willcock';

  public livingRoomBattery: number = -1;
  public diningRoomBattery: number = -1;
  public kitchenBattery: number = -1;
  public toiletBattery: number = -1;
  public bedroomBattery: number = -1;

  public lastMessageReceived: string = 'None';

  constructor(
    public http: HttpClient,
    public httpClientModule: HttpClientModule,
    public httpModule: HttpModule
    ) {
    console.log('Hello GlobalVariablesProvider Provider');
  }

}
